import React from 'react'
import { Responsive, WidthProvider } from 'react-grid-layout'

import HomeLeftMenu from '../../components/HomeLeftMenu';
import HomeTopNav from '../../components/HomeTopNav';

const ResponsiveGridLayout = WidthProvider(Responsive);

export default function Login() {

    const leftMenuWidth = 1.6
    const baseHeight = document.body.clientHeight / 150
    const topNavH = baseHeight / 12
    const lgCols = 12
    const mdCols = 12

    const leftMenuWidthPx = 1.6 / 12 * document.body.clientWidth

    const layouts = {
        lg: [
            { i: 'leftMenu', x: 0, y: 0, w: leftMenuWidth, h: baseHeight, static: true },
            { i: 'topNav', x: leftMenuWidth, y: 0, w: lgCols - leftMenuWidth, h: topNavH, static: true, minH: 0 },
            { i: 'context', x: leftMenuWidth, y: topNavH, w: lgCols - leftMenuWidth, h: baseHeight - topNavH, static: true }
        ],
        md: [
            { i: 'leftMenu', x: 0, y: 0, w: leftMenuWidth, h: baseHeight, static: true },
            { i: 'topNav', x: leftMenuWidth, y: 0, w: mdCols - leftMenuWidth, h: topNavH, static: true, minH: 0 },
            { i: 'context', x: leftMenuWidth, y: topNavH, w: mdCols - leftMenuWidth, h: baseHeight - topNavH, static: true }
        ]
    }

    return (
        <ResponsiveGridLayout
            layouts={layouts}
            breakpoints={{ lg: 1200, md: 996 }}
            cols={{ lg: lgCols, md: mdCols }}
            margin={[0, 0]}
        >
            <div key="leftMenu" style={{ backgroundColor: '#001529' }}>
                <HomeLeftMenu props={{ leftMenuWidthPx }} />
            </div>
            <div key="topNav" style={{ backgroundColor: 'white' }}>
                <HomeTopNav />
            </div>
            <div key="context" style={{ backgroundColor: 'pink' }}>context</div>
        </ResponsiveGridLayout >
    )
}
