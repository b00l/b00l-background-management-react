import { useState } from 'react'

import CanvasBg from '../../components/CanvasBg';
import LoginForm from '../../components/LoginForm';
import RegisterForm from '../../components/RegisterForm';

import './Login.scss'

export default function Login() {

    const [currentShowForm, setCurrentShowForm] = useState(1)

    return (
        <div className='login_page'>
            <CanvasBg />
            <div className='login_page_left_title slide-right5'>
                <h3>B00L-React通用管理系统快速开发框架</h3>
                <p>B00l-react 是基于现有的Ant Design库进行的二次封装，从而简化一些繁琐的操作，核心理念为数据驱动视图。</p>
            </div>
            <div className='login_page_right_form slide-left5'>
                {
                    currentShowForm ? <LoginForm props={{ setCurrentShowForm }} /> : <RegisterForm props={{ setCurrentShowForm }} />
                }

            </div>
        </div>

    )
}
