import React from 'react'
import { BrowserRouter, Route, Redirect } from 'react-router-dom'

import Login from './pages/Login'
import Home from './pages/Home'

interface route {
    path: string;
    component: any;
    children?: any
}

const routers: route[] = [
    { path: '/login', component: Login },
    { path: '/', component: Home }
]

export default function Router() {
    return (
        <BrowserRouter>
            {
                routers.map((route: any) => {
                    return (
                        <Route
                            exact
                            key={route.path}
                            path={route.path}
                            component={route.component}
                        ></Route>
                    )
                })
            }
        </BrowserRouter>
    )
}
