import React from 'react'
import './LoginForm.scss'
import { Form, Input, Button, Checkbox } from 'antd'

import VCode from '../../utils/VCode'

export default function LoginForm({ props }: any) {

    const Demo = () => {
        const onFinish = (values: any) => {
            console.log('Success:', values);
        };

        const onFinishFailed = (errorInfo: any) => {
            console.log('Failed:', errorInfo);
        };
    }

    const onFinish = () => {
        console.log('finish')
    }

    const handleRegister = () => {
        props.setCurrentShowForm(0)
    }

    return (
        <div className='login_form'>
            <h3>登陆B00L-React</h3>
            <div className='login_form_items'>
                <Form
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    autoComplete="off"
                >
                    <Form.Item
                        name="username"
                        initialValue='admin'
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        name="password"
                        initialValue='123456'
                    >
                        <Input.Password />
                    </Form.Item>

                    <VCode />

                    <Form.Item name="remember" style={{ display: 'inline-block', width: '140px' }} valuePropName="checked" wrapperCol={{ offset: 8, span: 16 }}>
                        <Checkbox className='lfi_remember'>记住用户名</Checkbox>
                    </Form.Item>
                    <span className='lfi_register' onClick={handleRegister} >没有账号，立即注册</span>

                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            登陆
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
}
