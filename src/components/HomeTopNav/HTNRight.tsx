import { Badge, Avatar } from 'antd';

import { FullscreenOutlined, BellOutlined } from '@ant-design/icons';

export default function HTNRight() {
    return (
        <div className="HTNRight">
            <div className="htnr_notice">
                <Badge count={5}>
                    <BellOutlined />
                </Badge>
            </div>
            <div className="htnr_fullscreen">
                <FullscreenOutlined />
            </div>
            <div className="htnr_user">
                <Avatar size="large" src='https://z3.ax1x.com/2021/10/25/54QOPJ.jpg' />
            </div>
        </div>
    )
}
