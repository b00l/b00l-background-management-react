import { Input } from 'antd';

export default function HTNLeft() {

    const onSearch = () => {
        console.log('搜索')
    }

    return (
        <Input.Search placeholder="input search text" onSearch={onSearch} enterButton />
    )
}
