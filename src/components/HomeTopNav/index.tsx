import React from 'react'

import HTNLeft from './HTNLeft';
import HTNRight from './HTNRight';

import './HomeTopNav.scss'

export default function HomeTopNav() {
    return (
        <div className='home_top_nav'>
            <div className="htn_left">
                <HTNLeft />
            </div>
            <div className="htn_right">
                <HTNRight />
            </div>
        </div>
    )
}
