import { useState } from 'react';
import { Menu, Button } from 'antd';
import {
    PieChartOutlined,
    ExclamationCircleOutlined,
    IeOutlined,
    TableOutlined,
    GlobalOutlined
} from '@ant-design/icons';

const { SubMenu } = Menu;

export default function HomeLeftMenu({ props }: any) {

    const [collapsed, setCollapsed] = useState<Boolean>(false)

    const toggleCollapsed = () => {
        setCollapsed(!collapsed)
    };
    
    return (
        <div style={{ width: props.leftMenuWidthPx }}>
            <Button type="primary" onClick={toggleCollapsed} style={{ marginBottom: 16 }}>
            </Button>
            <Menu
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                mode="inline"
                theme="dark"
            >
                <Menu.Item key="1" icon={<IeOutlined />}>
                    首页
                </Menu.Item>
                <Menu.Item key="2" icon={<PieChartOutlined />}>
                    数据展示
                </Menu.Item>
                <Menu.Item key="3" icon={<TableOutlined />}>
                    表格
                </Menu.Item>
                <SubMenu key="sub1" icon={<GlobalOutlined />} title="第三方网站">
                    <Menu.Item key="5">吾为极客</Menu.Item>
                </SubMenu>
                <Menu.Item key="4" icon={<ExclamationCircleOutlined />}>
                    关于本站
                </Menu.Item>
            </Menu>
        </div>
    );
}